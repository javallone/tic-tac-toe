# Tic-Tac-Toe

* `yarn build` - Build app into `dist/`.
* `yarn start` - Start development server on http://localhost:9000/. Port can be
    changed by setting the `PORT` environment variable.
* `yarn test:lint` - Run Eslint on the project. Eslint will also automatically
    be run by a pre-commit hook managed by Husky
* `yarn test:unit` - Run Jest tests.
