/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  coverageReporters: [
    'html',
    'text-summary'
  ],
  errorOnDeprecated: true,
  moduleNameMapper: {
    '\\.css$': 'identity-obj-proxy'
  },
  modulePaths: [
    '<rootDir>/src'
  ],
  notify: true,
  setupFilesAfterEnv: [
    '<rootDir>/src/setup-tests.js'
  ],
  snapshotSerializers: [
    'enzyme-to-json/serializer'
  ],
  timers: 'fake',
  transformIgnorePatterns: [
    '\\/node_modules\\/(?!.*\\.svg$)'
  ],
  transform: {
    '\\.js$': 'babel-jest',
    '\\.svg$': '<rootDir>/__mocks__/svg.js'
  }
};
