const path = require('path');

module.exports = {
  process(src, filename) {
    const displayName = `SVG:${path.basename(filename)}`;
    return `
      module.exports = () => ${JSON.stringify(displayName)};
      module.exports.displayName = ${JSON.stringify(displayName)};
    `;
  }
};
