const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const PwaManifest = require('webpack-pwa-manifest');

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  mode: isProd ? 'production' : 'development',
  entry: './src/main.js',
  output: {
    filename: '[name]-[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: './'
  },
  resolve: {
    modules: [
      path.resolve(__dirname, 'src'),
      'node_modules'
    ]
  },
  target: 'web',
  devtool: 'source-map',
  devServer: {
    contentBase: './dist',
    port: process.env.PORT || '9000'
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name]-[contenthash].css'
    }),
    new webpack.EnvironmentPlugin({
      CI_COMMIT_SHORT_SHA: '##sha##',
      NODE_ENV: 'development',
      BUILD_TIME: new Date().toISOString()
    }),
    isProd && new WorkboxPlugin.GenerateSW(),
    new PwaManifest({
      inject: true,
      name: 'Tic-Tac-Toe',
      display: 'standalone',
      orientation: 'omit',
      background_color: '#333',
      start_url: './index.html',
      icons: [
        {
          src: path.resolve(__dirname, 'src/icon.svg'),
          sizes: [144]
        }
      ]
    })
  ].filter(Boolean),
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'react-svg-loader',
            options: {
              svgo: {
                plugins: [
                  { removeViewBox: false },
                  { removeDimensions: true }
                ]
              }
            }
          }
        ]
      }
    ]
  }
};
