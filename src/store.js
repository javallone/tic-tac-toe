import React, { useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

import reducers from 'reducers';
import { initialize } from 'actions';

const reducer = (state, { type, ...action }) => {
  if (reducers[type]) {
    return reducers[type](state, action);
  }

  console.log('Unknown action type:', type, action);
  return state;
};

const StoreContext = React.createContext();
const DispatchContext = React.createContext();

export const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, {});

  if (!state.initialized) {
    dispatch(initialize());
    return;
  }

  return <StoreContext.Provider value={ state }>
    <DispatchContext.Provider value={ dispatch }>
      { children }
    </DispatchContext.Provider>
  </StoreContext.Provider>;
};

StoreProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export const useStore = () => {
  return useContext(StoreContext);
};

export const useDispatch = () => {
  return useContext(DispatchContext);
};
