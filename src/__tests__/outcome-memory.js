import OutcomeMemory from 'outcome-memory';
import { saveStorage, loadStorage, removeStorage } from 'storage';
import { PLAYER_NONE, PLAYER_X, PLAYER_O } from 'app-constants';

const KEY = 'test-storage-key';

describe('OutcomeMemory', () => {
  let storage;

  beforeEach(() => {
    storage = new OutcomeMemory(KEY);

    jest.spyOn(console, 'log').mockImplementation(() => {});
  });

  afterEach(() => {
    removeStorage(KEY);
  });

  describe('loadStorage', () => {
    it('loads the learned outcomes', () => {
      saveStorage(KEY, { 'hash': 1 });
      storage.loadStorage();
      expect(storage.memory).toEqual({ 'hash': 1 });
    });
  });

  describe('saveStorage', () => {
    it('saves the learned outcomes', () => {
      storage.memory = { 'hash': -1 };
      storage.saveStorage();
      expect(loadStorage(KEY)).toEqual({ 'hash': -1 });
    });
  });

  describe('clearStorage', () => {
    it('clears the learned outcomes', () => {
      storage.loadStorage();
      storage.learn({ hash: '1234' }, PLAYER_NONE);
      expect(loadStorage(KEY)).toEqual({ '1234': 0 });
      expect(storage.memory).toEqual({ '1234': 0 });
      storage.clearStorage();
      expect(loadStorage(KEY)).toBeNull();
      expect(storage.memory).toEqual({});
    });
  });

  describe('learn', () => {
    beforeEach(() => {
      storage.loadStorage();
      jest.spyOn(storage, 'saveStorage');
    });

    it('updates the memory', () => {
      storage.learn({ hash: '1234' }, PLAYER_X);
      storage.learn({ hash: '5678' }, PLAYER_O);
      storage.learn({ hash: '0000' }, PLAYER_NONE);
      expect(storage.memory).toEqual({ '1234': 1, '5678': -1, '0000': 0 });
    });

    it('saves the memory', () => {
      storage.learn({ hash: '1234' }, PLAYER_X);
      expect(storage.saveStorage).toHaveBeenCalled();
    });

    it('does not save storage or update memory if state is falsy', () => {
      storage.learn(null, PLAYER_X);
      expect(storage.memory).toEqual({});
      expect(storage.saveStorage).not.toHaveBeenCalled();
    });

    it('does not save storage or update memory if state outcome is already learned', () => {
      storage.memory['1234'] = 1;
      storage.learn({ hash: '1234' }, PLAYER_X);
      expect(storage.memory).toEqual({ '1234': 1 });
      expect(storage.saveStorage).not.toHaveBeenCalled();
    });

    it('saves storage and updates memory if state outcome is to be relearned', () => {
      storage.memory['1234'] = 0;
      storage.learn({ hash: '1234' }, PLAYER_X);
      expect(storage.memory).toEqual({ '1234': 1 });
      expect(storage.saveStorage).toHaveBeenCalled();
    });
  });

  describe('outcome', () => {
    beforeEach(() => {
      storage.loadStorage();
    });

    it('returns the state winner when state is complete', () => {
      expect(storage.outcome({ isComplete: true, winner: PLAYER_X })).toEqual(PLAYER_X);
    });

    it('returns the stored outcome', () => {
      const state = { hash: '1234', isComplete: false };
      storage.learn(state, PLAYER_O);
      expect(storage.outcome(state)).toEqual(PLAYER_O);
    });

    it('returned `undefined` for unknown outcomes', () => {
      expect(storage.outcome({ hash: '1234' })).toBeUndefined();
    });
  });
});
