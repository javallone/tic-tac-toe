import { PLAYER_X, PLAYER_O, PLAYER_NONE } from 'app-constants';
import GameState from 'game-state';
import { strToBoard } from 'test-helpers';

describe('GameState', () => {
  let emptyBoard;

  beforeEach(() => {
    emptyBoard = strToBoard('... ... ...');
  });

  it('caches game state instances', () => {
    // Using `toBe` to test identity, not just equality
    expect(emptyBoard).toBe(new GameState());
  });

  describe('player', () => {
    it('is PLAYER_X for the empty board', () => {
      expect(emptyBoard.player).toEqual(PLAYER_X);
    });

    it('is PLAYER_O if X has most recently moved', () => {
      expect(emptyBoard.moveAt(0).player).toEqual(PLAYER_O);
    });

    it('is PLAYER_X if O has most recently moved', () => {
      expect(emptyBoard.moveAt(0).moveAt(1).player).toEqual(PLAYER_X);
    });
  });

  describe('isComplete', () => {
    it('is false if the game is not finished', () => {
      expect(emptyBoard.isComplete).toEqual(false);
    });

    it('is true if there is a winner', () => {
      const board = strToBoard('XXX OO. ...');
      expect(board.isComplete).toEqual(true);
    });

    it('is true if the game is a stalemate', () => {
      const board = strToBoard('XOX OOX XXO');
      expect(board.isComplete).toEqual(true);
    });
  });

  describe('winner', () => {
    it('is PLAYER_NONE if the game is not finished', () => {
      expect(emptyBoard.winner).toEqual(PLAYER_NONE);
    });

    it('is PLAYER_NONE if the game is a stalemate', () => {
      const board = strToBoard('XOX OOX XXO');
      expect(board.winner).toEqual(PLAYER_NONE);
    });

    it('is PLAYER_X if X won', () => {
      const board = strToBoard('XXX OO. ...');
      expect(board.winner).toEqual(PLAYER_X);
    });

    it('is PLAYER_O if O won', () => {
      const board = strToBoard('XX. OOO X..');
      expect(board.winner).toEqual(PLAYER_O);
    });
  });

  describe('winDirection', () => {
    [
      ['XXX ... ...', 'row0'],
      ['... XXX ...', 'row1'],
      ['... ... XXX', 'row2'],
      ['X.. X.. X..', 'col0'],
      ['.X. .X. .X.', 'col1'],
      ['..X ..X ..X', 'col2'],
      ['X.. .X. ..X', 'diaga'],
      ['..X .X. X..', 'diagb']
    ].forEach(([str, winDirection]) => {
      it(`is "${winDirection}" for ${str}`, () => {
        const board = strToBoard(str);
        expect(board.winDirection).toEqual(winDirection);
      });
    });
  });

  describe('isStalemate', () => {
    it('is false if the game is not finished', () => {
      expect(emptyBoard.isStalemate).toEqual(false);
    });

    it('is false if the game has a winner', () => {
      const board = strToBoard('XXX OO. ...');
      expect(board.isStalemate).toEqual(false);
    });

    it('is true if the game is a stalemate', () => {
      const board = strToBoard('XOX OOX XXO');
      expect(board.isStalemate).toEqual(true);
    });
  });

  describe('hash', () => {
    it('is equal for boards that have rotational or mirror symmetry', () => {
      const boards = [
        'XO. ... ...',
        '..X ..O ...',
        '... ... .OX',
        '... O.. X..',
        '.OX ... ...',
        'X.. O.. ...',
        '... ... XO.',
        '... ..O ..X'
      ].map((s) => strToBoard(s));

      expect(boards[0].hash).toEqual(boards[1].hash);
      expect(boards[0].hash).toEqual(boards[2].hash);
      expect(boards[0].hash).toEqual(boards[3].hash);
      expect(boards[0].hash).toEqual(boards[4].hash);
      expect(boards[0].hash).toEqual(boards[5].hash);
      expect(boards[0].hash).toEqual(boards[6].hash);
      expect(boards[0].hash).toEqual(boards[7].hash);
    });
  });

  describe('moves', () => {
    it('returns a list of game states that can be reacted from this state', () => {
      expect(emptyBoard.moveAt(0).moves).toMatchSnapshot();
    });

    it('returns an empty list for games that are finished', () => {
      const board = strToBoard('XXX ... ...');
      expect(board.moves).toEqual([]);
    });

    it('caches move list', () => {
      // Using `toBe` to test identity, not just equality
      expect(emptyBoard.moves).toBe(new GameState().moves);
    });
  });

  describe('score', () => {
    it('equals 1 if the winner is X', () => {
      const board = strToBoard('XXX ... ...');
      expect(board.score).toEqual(1);
    });

    it('equals -1 if the winner is O', () => {
      const board = strToBoard('OOO ... ...');
      expect(board.score).toEqual(-1);
    });

    it('equals 0 if the game is a stalemate', () => {
      const board = strToBoard('XOX OOX XXO');
      expect(board.score).toEqual(0);
    });

    it('equals 1 if it X can win', () => {
      const board = strToBoard('XX. OO. ...');
      expect(board.score).toEqual(1);
    });

    it('equals -1 if it O can win', () => {
      const board = strToBoard('OO. XX. X..');
      expect(board.score).toEqual(-1);
    });

    it('equals the average score of all possible moves', () => {
      const board = strToBoard('XOX O.X X.O');
      expect(board.score).toEqual(0.5);
    });
  });

  describe('playerAt', () => {
    it('returns the player at a given position', () => {
      const board = strToBoard('XO. ... ...');
      expect(board.playerAt(0)).toEqual(PLAYER_X);
      expect(board.playerAt(1)).toEqual(PLAYER_O);
      expect(board.playerAt(2)).toEqual(PLAYER_NONE);
    });
  });

  describe('moveAt', () => {
    it('returns a new game state with the current player at the given position', () => {
      expect(emptyBoard.moveAt(0)).toMatchSnapshot();
    });

    it('throws an exception if the game is finished', () => {
      const board = strToBoard('XXX OO. ...');
      expect(() => board.moveAt(5)).toThrow('Attempted to move after complete');
    });

    it('throws an excpetion if space is occupied', () => {
      const board = strToBoard('X.. ... ...');
      expect(() => board.moveAt(0)).toThrow('Attempted to move in occupied space');
    });
  });
});
