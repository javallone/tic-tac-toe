import { loadStorage, saveStorage, removeStorage } from 'storage';

describe('localStorage methods', () => {
  beforeEach(() => {
    jest.spyOn(localStorage.__proto__, 'getItem');
    jest.spyOn(localStorage.__proto__, 'setItem');
    jest.spyOn(localStorage.__proto__, 'removeItem');
  });

  describe('loadStorage', () => {
    it('returns the fallback when no data is available', () => {
      expect(loadStorage('example', { fallback: 'content' })).toEqual({ fallback: 'content' });
    });

    it('returns the stored content when data is available', () => {
      localStorage.setItem('example', '{"testing":"content"}');
      expect(loadStorage('example', { fallback: 'content' })).toEqual({ testing: 'content' });
    });

    it('returns the fallback when the stored data is unparsable', () => {
      localStorage.setItem('example', '{"testing":"content"');
      expect(loadStorage('example', { fallback: 'content' })).toEqual({ fallback: 'content' });
    });
  });

  describe('saveStorage', () => {
    it('saves the value to localStorage', () => {
      saveStorage('example', { testing: 'content' });
      expect(localStorage.setItem).toHaveBeenCalledWith('example', '{"testing":"content"}');
    });
  });

  describe('removeStorage', () => {
    it('removes the item from localStorage', () => {
      removeStorage('example');
      expect(localStorage.removeItem).toHaveBeenCalledWith('example');
    });
  });
});
