import reducers from 'reducers';
import {
  SETTINGS_STORE,
  CHANGE_SETTINGS,
  CHANGE_STATE,
  INITIALIZE,
  SET_PAUSED,
  START_GAME
} from 'app-constants';
import {
  loadStorage,
  saveStorage
} from 'storage';
import OutcomeMemory from 'outcome-memory';
import nameGenerator from 'name-generator';

jest.mock('storage');
jest.mock('name-generator');

describe('reducers', () => {
  let state;

  beforeEach(() => {
    state = {
      gameState: { example: 'state' },
      memory: {
        loadStorage: jest.fn()
      },
      settings: {
        players: [
          { type: 'human', name: 'Player 1' },
          { type: 'human', name: 'Player 2' }
        ],
        randomPlay: false
      },
      paused: false
    };

    let count = 0;
    nameGenerator.mockImplementation(() => `Random name ${count++}`);
  });

  describe('CHANGE_SETTINGS', () => {
    it('saves the settings to localStorage', () => {
      reducers[CHANGE_SETTINGS](state, { settings: { example: 'new settings' } });
      expect(saveStorage).toHaveBeenCalledWith(SETTINGS_STORE, { example: 'new settings' });
    });

    it('set the settings', () => {
      expect(reducers[CHANGE_SETTINGS](state, { settings: { example: 'new settings' } })).toMatchSnapshot();
    });
  });

  describe('CHANGE_STATE', () => {
    it('sets the gameState', () => {
      expect(reducers[CHANGE_STATE](state, { gameState: { example: 'new state' } })).toMatchSnapshot();
    });

    it('does not change gameState when provided state is falsy', () => {
      expect(reducers[CHANGE_STATE](state, { gameState: undefined })).toMatchSnapshot();
    });
  });

  describe('INITIALIZE', () => {
    beforeEach(() => {
      jest.spyOn(OutcomeMemory.prototype, 'loadStorage').mockImplementation(() => {});
    });

    it('initializes the store with a new game', () => {
      expect(reducers[INITIALIZE]({})).toMatchSnapshot();
    });

    it('loads the outcome memory', () => {
      const { memory } = reducers[INITIALIZE]({});
      expect(memory.loadStorage).toHaveBeenCalled();
    });

    it('loads saved settings', () => {
      jest.spyOn(Math, 'random');
      Math.random.mockReturnValue(0.1);

      loadStorage.mockReturnValue({
        players: [
          { type: 'expert', name: 'Player 1' },
          { type: 'simple', name: 'Player 2' }
        ],
        randomPlay: true,
        resetDelay: 1000
      });
      expect(reducers[INITIALIZE]({})).toMatchSnapshot();
    });

    it('does not change the state if it has been initailized', () => {
      expect(reducers[INITIALIZE]({ initialized: true, testing: 'state' })).toMatchSnapshot();
    });
  });

  describe('SET_PAUSED', () => {
    it('sets the paused state', () => {
      expect(reducers[SET_PAUSED](state, { paused: true })).toMatchSnapshot();
    });
  });

  describe('START_GAME', () => {
    it('sets up gameState and players with player 1 as X', () => {
      expect(reducers[START_GAME](state)).toMatchSnapshot();
    });

    it('sets up gameState and players with player 1 or 2 as X when randomPlay is enabled', () => {
      state.settings.randomPlay = true;
      jest.spyOn(Math, 'random');

      Math.random.mockReturnValue(0.1);
      expect(reducers[START_GAME](state)).toMatchSnapshot();
      Math.random.mockReturnValue(0.9);
      expect(reducers[START_GAME](state)).toMatchSnapshot();
    });
  });
});
