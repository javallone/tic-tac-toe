import React, { mockReducer } from 'react';
import { shallow, mount } from 'enzyme';

import {
  INITIALIZE
} from 'app-constants';
import reducers from 'reducers';
import { StoreProvider, useStore, useDispatch } from 'store';

jest.mock('react', () => {
  let reducerState;
  const originalReact = jest.requireActual('react');

  return {
    ...originalReact,
    useReducer: jest.fn().mockImplementation((reducer, initialState) => {
      if (!reducerState) {
        reducerState = initialState;
      }

      const dispatch = (action) => reducerState = reducer(reducerState, action);
      return [reducerState, dispatch];
    }),
    mockReducer: {
      get state() {
        return reducerState;
      },

      set state(value) {
        reducerState = value;
      }
    }
  };
});

const ExportComponent = React.forwardRef((props, ref) => {
  const store = useStore();
  const dispatch = useDispatch();

  ref.current = { store, dispatch };

  return null;
});
ExportComponent.displayName = 'ExportComponent';

describe('<StoreProvider>', () => {
  beforeEach(() => {
    mockReducer.state = {
      initialized: true
    };
  });

  it('renders children', () => {
    const wrapper = shallow(
      <StoreProvider>
        <div>Children</div>
      </StoreProvider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('dispatches an initialize action when not initialized', () => {
    jest.spyOn(reducers, INITIALIZE).mockImplementation(() => {});
    mockReducer.state.initialized = false;

    const wrapper = shallow(
      <StoreProvider>
        <div>Children</div>
      </StoreProvider>
    );
    expect(wrapper).toMatchSnapshot();
    expect(reducers[INITIALIZE]).toHaveBeenCalledWith({ initialized: false }, {});
  });
});

describe('useStore', () => {
  beforeEach(() => {
    mockReducer.state = {
      initialized: true
    };
  });

  it('returns the current store value', () => {
    mockReducer.state.example = 'testing';

    const ref = React.createRef();
    mount(
      <StoreProvider>
        <ExportComponent ref={ ref }/>
      </StoreProvider>
    );

    expect(ref.current.store).toEqual({
      initialized: true,
      example: 'testing'
    });
  });
});

describe('useDispatch', () => {
  beforeEach(() => {
    mockReducer.state = {
      initialized: true
    };
  });

  it('calls the appropriate reducer', () => {
    const ref = React.createRef();
    mount(
      <StoreProvider>
        <ExportComponent ref={ ref }/>
      </StoreProvider>
    );

    jest.spyOn(reducers, INITIALIZE);
    ref.current.dispatch({ type: INITIALIZE, other: 'props' });
    expect(reducers[INITIALIZE]).toHaveBeenCalledWith({ initialized: true }, { other: 'props' });
  });

  it('properly handles unknown reducers', () => {
    jest.spyOn(console, 'log').mockImplementation(() => {});

    const ref = React.createRef();
    mount(
      <StoreProvider>
        <ExportComponent ref={ ref }/>
      </StoreProvider>
    );

    ref.current.dispatch({ type: 'UNKNOWN' });
    expect(mockReducer.state).toEqual({ initialized: true });
    expect(console.log).toHaveBeenCalledWith('Unknown action type:', 'UNKNOWN', {});
  });
});
