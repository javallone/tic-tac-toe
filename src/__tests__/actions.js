import {
  changeSettings,
  changeState,
  initialize,
  setPaused,
  startGame
} from 'actions';

describe('actions', () => {
  describe('changeSettings', () => {
    it('creates a CHANGE_SETTINGS action', () => {
      expect(changeSettings({ example: 'settings' })).toMatchSnapshot();
    });
  });

  describe('changeState', () => {
    it('creates a CHANGE_STATE action', () => {
      expect(changeState({ example: 'state' })).toMatchSnapshot();
    });
  });

  describe('initialize', () => {
    it('creates an INITIALIZE action', () => {
      expect(initialize()).toMatchSnapshot();
    });
  });

  describe('setPaused', () => {
    it('creates a SET_PAUSED action', () => {
      expect(setPaused(true)).toMatchSnapshot();
      expect(setPaused(false)).toMatchSnapshot();
    });
  });

  describe('startGame', () => {
    it('creates a START_GAME action', () => {
      expect(startGame()).toMatchSnapshot();
    });
  });
});
