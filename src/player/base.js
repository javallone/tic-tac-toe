import { PLAYER_X, PLAYER_O } from 'app-constants';

export const chooseRandom = (moves) => moves[Math.floor(Math.random() * moves.length)];

class Base {
  constructor(player, { name }) {
    this.player = player;
    this.opponent = player === PLAYER_X ? PLAYER_O : PLAYER_X;
    this.name = name;
    this.status = null;
  }

  clickHandler() {}

  move() {}

  complete() {}
}

export default Base;
