import LearningComputerPlayer from 'player/learning-computer-player';
import { PLAYER_X, PLAYER_O, PLAYER_NONE } from 'app-constants';

describe('LearningComputerPlayer', () => {
  let players;
  let memory;

  beforeEach(() => {
    memory = {
      learn: jest.fn(),
      outcome: (state) => state.outcome
    };
    players = {
      [PLAYER_X]: new LearningComputerPlayer(PLAYER_X, { name: 'Testing', memory }),
      [PLAYER_O]: new LearningComputerPlayer(PLAYER_O, { name: 'Testing', memory })
    };
    players[PLAYER_X].lastMove = 'testing last move';
    players[PLAYER_O].lastMove = 'testing last move';
  });

  describe('move', () => {
    [
      { player: PLAYER_X, opponent: PLAYER_O, desc: 'Playing as X' },
      { player: PLAYER_O, opponent: PLAYER_X, desc: 'Playing as O' }
    ].forEach(({ player, opponent, desc }) => {
      describe(`${desc}`, () => {
        it('chooses a winning move when possible and does not learn', () => {
          const gameState = {
            moves: [
              { id: 0, outcome: player },
              { id: 1, outcome: opponent },
              { id: 2, outcome: PLAYER_NONE },
              { id: 3 }
            ]
          };
          expect(players[player].move(gameState)).toEqual({ id: 0, outcome: player });
          expect(players[player].lastMove).toEqual({ id: 0, outcome: player });
          expect(players[player].status).toEqual('Expects to win');
          expect(memory.learn).not.toHaveBeenCalled();
        });

        it('chooses a move with unknown outcome when possible and does not learn', () => {
          const gameState = {
            moves: [
              { id: 1, outcome: opponent },
              { id: 2, outcome: PLAYER_NONE },
              { id: 3 }
            ]
          };
          expect(players[player].move(gameState)).toEqual({ id: 3 });
          expect(players[player].lastMove).toEqual({ id: 3 });
          expect(players[player].status).toEqual('No expectation');
          expect(memory.learn).not.toHaveBeenCalled();
        });

        it('chooses a move that stalemates when possible and learns the outcome', () => {
          const gameState = {
            moves: [
              { id: 1, outcome: opponent },
              { id: 2, outcome: PLAYER_NONE }
            ]
          };
          expect(players[player].move(gameState)).toEqual({ id: 2, outcome: PLAYER_NONE });
          expect(players[player].lastMove).toEqual({ id: 2, outcome: PLAYER_NONE });
          expect(players[player].status).toEqual('Expects to stalemate');
          expect(memory.learn).toHaveBeenCalledWith('testing last move', PLAYER_NONE);
        });

        it('chooses a losing move when necessary and learns the outcome', () => {
          const gameState = {
            moves: [
              { id: 1, outcome: opponent }
            ]
          };
          expect(players[player].move(gameState)).toEqual({ id: 1, outcome: opponent });
          expect(players[player].lastMove).toEqual({ id: 1, outcome: opponent });
          expect(players[player].status).toEqual('Expects to lose');
          expect(memory.learn).toHaveBeenCalledWith('testing last move', opponent);
        });
      });
    });
  });

  describe('complete', () => {
    [
      { player: PLAYER_X, opponent: PLAYER_O, desc: 'Playing as X' },
      { player: PLAYER_O, opponent: PLAYER_X, desc: 'Playing as O' }
    ].forEach(({ player, opponent, desc }) => {
      describe(`${desc}`, () => {
        it('learns the last move outcome when the game is a stalemate', () => {
          players[player].complete({ winner: PLAYER_NONE });
          expect(memory.learn).toHaveBeenCalledWith('testing last move', PLAYER_NONE);
          expect(players[player].lastMove).toBeNull();
        });

        it('learns the outcome when the game is lost', () => {
          players[player].complete({ winner: opponent });
          expect(memory.learn).toHaveBeenCalledWith('testing last move', opponent);
          expect(players[player].lastMove).toBeNull();
        });

        it('does not learn the outcome when the game is won', () => {
          players[player].complete({ winner: player });
          expect(memory.learn).not.toHaveBeenCalled();
          expect(players[player].lastMove).toBeNull();
        });
      });
    });
  });
});
