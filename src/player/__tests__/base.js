import Base from 'player/base';
import { PLAYER_X, PLAYER_O } from 'app-constants';

describe('Base player class', () => {
  it('has the player, opponent, and player name', () => {
    const playerX = new Base(PLAYER_X, { name: 'Player X' });
    const playerO = new Base(PLAYER_O, { name: 'Player O' });

    expect(playerX).toMatchObject({ player: PLAYER_X, opponent: PLAYER_O, name: 'Player X' });
    expect(playerO).toMatchObject({ player: PLAYER_O, opponent: PLAYER_X, name: 'Player O' });
  });

  describe('clickHandler', () => {
    it('returns nothing', () => {
      const player = new Base(PLAYER_X, { name: 'Testing' });
      expect(player.clickHandler()).toBeUndefined();
    });
  });

  describe('move', () => {
    it('returns nothing', () => {
      const player = new Base(PLAYER_X, { name: 'Testing' });
      expect(player.move()).toBeUndefined();
    });
  });

  describe('complete', () => {
    it('returns nothing', () => {
      const player = new Base(PLAYER_X, { name: 'Testing' });
      expect(player.complete()).toBeUndefined();
    });
  });
});
