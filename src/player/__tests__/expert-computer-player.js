import ExpertComputerPlayer from 'player/expert-computer-player';
import { PLAYER_X, PLAYER_O } from 'app-constants';

describe('ExpertComputerPlayer', () => {
  describe('move', () => {
    it('chooses a move with the highest score when playing as X', () => {
      const player = new ExpertComputerPlayer(PLAYER_X, { name: 'Testing' });
      const gameState = {
        moves: [
          { id: 0, score: -1 },
          { id: 1, score: 1 },
          { id: 2, score: 0 },
          { id: 3, score: -1 },
          { id: 4, score: 1 }
        ]
      };
      const newState = player.move(gameState);
      expect(newState.score).toEqual(1);
      expect(player.status).toEqual('Score: 1');
    });

    it('chooses a move with the lowest score when playing as O', () => {
      const player = new ExpertComputerPlayer(PLAYER_O, { name: 'Testing' });
      const gameState = {
        moves: [
          { id: 0, score: -1 },
          { id: 1, score: 1 },
          { id: 2, score: 0 },
          { id: 3, score: -1 },
          { id: 4, score: 1 }
        ]
      };
      const newState = player.move(gameState);
      expect(newState.score).toEqual(-1);
      expect(player.status).toEqual('Score: -1');
    });
  });
});
