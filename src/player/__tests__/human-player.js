import HumanPlayer from 'player/human-player';
import { PLAYER_X } from 'app-constants';

describe('HumanPlayer', () => {
  let player;

  beforeEach(() => {
    player = new HumanPlayer(PLAYER_X, { name: 'Testing' });
  });

  describe('clickHandler', () => {
    it('moves at the provided position', () => {
      const state = { moveAt: jest.fn().mockReturnValue('new state') };
      expect(player.clickHandler({ x: 1, y: 2 }, state)).toEqual('new state');
      expect(state.moveAt).toHaveBeenCalledWith({ x: 1, y: 2 });
    });
  });
});
