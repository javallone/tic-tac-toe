import SimpleComputerPlayer from 'player/simple-computer-player';
import { PLAYER_X, PLAYER_O, PLAYER_NONE } from 'app-constants';

describe('SimpleComputerPlayer', () => {
  let player;

  beforeEach(() => {
    player = new SimpleComputerPlayer(PLAYER_X, { name: 'Testing' });
  });

  describe('move', () => {
    it('chooses a winning move if available', () => {
      const gameState = {
        moves: [
          { winner: PLAYER_X, moves: [] },
          { winner: PLAYER_NONE, moves: [] }
        ]
      };
      expect(player.move(gameState)).toEqual({ winner: PLAYER_X, moves: [] });
    });

    it('chooses a move that does not lead to losing', () => {
      const gameState = {
        moves: [
          { winner: PLAYER_NONE, moves: [{ winner: PLAYER_NONE }] },
          { winner: PLAYER_NONE, moves: [{ winner: PLAYER_O }] }
        ]
      };
      expect(player.move(gameState)).toEqual({ winner: PLAYER_NONE, moves: [{ winner: PLAYER_NONE }] });
    });

    it('chooses any move', () => {
      const gameState = {
        moves: [
          { winner: PLAYER_NONE, moves: [{ winner: PLAYER_O }] }
        ]
      };
      expect(player.move(gameState)).toEqual({ winner: PLAYER_NONE, moves: [{ winner: PLAYER_O }] });
    });
  });
});
