import Base, { chooseRandom } from 'player/base';

class SimpleComputerPlayer extends Base {
  static description = 'Simple Computer'

  move(state) {
    const wins = state.moves.filter((m) => m.winner === this.player);
    const safe = state.moves.filter((m) => !m.moves.find((x) => x.winner === this.opponent));

    return chooseRandom(wins)
      || chooseRandom(safe)
      || chooseRandom(state.moves);
  }
}

export default SimpleComputerPlayer;
