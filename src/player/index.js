import HumanPlayer from 'player/human-player';
import SimpleComputerPlayer from 'player/simple-computer-player';
import LearningComputerPlayer from 'player/learning-computer-player';
import ExpertComputerPlayer from 'player/expert-computer-player';

export const PLAYER_CLASS = Object.freeze({
  human: HumanPlayer,
  simple: SimpleComputerPlayer,
  learning: LearningComputerPlayer,
  expert: ExpertComputerPlayer
});
