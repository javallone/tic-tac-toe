import Base, { chooseRandom } from 'player/base';

import { PLAYER_X, PLAYER_O, PLAYER_NONE } from 'app-constants';

const UNKNOWN = Symbol('UNKNOWN');

const STATUS = {
  [PLAYER_X]: {
    [PLAYER_X]: 'Expects to win',
    [PLAYER_O]: 'Expects to lose',
    [PLAYER_NONE]: 'Expects to stalemate',
    [UNKNOWN]: 'No expectation'
  },
  [PLAYER_O]: {
    [PLAYER_X]: 'Expects to lose',
    [PLAYER_O]: 'Expects to win',
    [PLAYER_NONE]: 'Expects to stalemate',
    [UNKNOWN]: 'No expectation'
  }
};
const OUTCOME_PRIORITY = {
  [PLAYER_X]: [
    PLAYER_X,
    UNKNOWN,
    PLAYER_NONE,
    PLAYER_O
  ],
  [PLAYER_O]: [
    PLAYER_O,
    UNKNOWN,
    PLAYER_NONE,
    PLAYER_X
  ]
};
const LEARN_OUTCOMES = {
  [PLAYER_X]: [PLAYER_O, PLAYER_NONE],
  [PLAYER_O]: [PLAYER_X, PLAYER_NONE]
};

class LearningComputerPlayer extends Base {
  static description = 'Learning Computer'

  constructor(player, { memory, ...options }) {
    super(player, options);
    this.memory = memory;
    this.lastMove = null;
  }

  move(state) {
    const groupedMoves = state.moves.reduce((grouped, m) => {
      const outcome = this.memory.outcome(m) || UNKNOWN;
      grouped[outcome].push(m);
      return grouped;
    }, {
      [this.player]: [],
      [this.opponent]: [],
      [PLAYER_NONE]: [],
      [UNKNOWN]: []
    });
    const expectedOutcome = OUTCOME_PRIORITY[this.player].find((s) => groupedMoves[s].length);

    if (LEARN_OUTCOMES[this.player].includes(expectedOutcome)) {
      this.memory.learn(this.lastMove, expectedOutcome);
    }

    this.status = STATUS[this.player][expectedOutcome];

    this.lastMove = chooseRandom(groupedMoves[expectedOutcome]);
    return this.lastMove;
  }

  complete(state) {
    if (LEARN_OUTCOMES[this.player].includes(state.winner)) {
      this.memory.learn(this.lastMove, state.winner);
    }

    this.lastMove = null;
  }
}

export default LearningComputerPlayer;
