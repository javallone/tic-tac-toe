import Base from 'player/base';

class HumanPlayer extends Base {
  static description = 'Human'

  clickHandler(pos, state) {
    return state.moveAt(pos);
  }
}

export default HumanPlayer;
