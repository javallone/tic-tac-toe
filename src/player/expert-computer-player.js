import { PLAYER_TO_BIPOLAR } from 'app-constants';
import Base from 'player/base';

class ExpertComputerPlayer extends Base {
  static description = 'Expert Computer'

  move(state) {
    const priorityMoves = [...state.moves].sort((a, b) => {
      const aScore = a.score;
      const bScore = b.score;

      if (aScore === bScore) {
        return Math.floor(Math.random() * 3) - 1; // -1, 0, or 1
      }

      return PLAYER_TO_BIPOLAR[this.player] * (aScore < bScore ? 1 : -1);
    });

    this.status = `Score: ${Math.round(priorityMoves[0].score * 1000) / 1000}`;

    return priorityMoves[0];
  }
}

export default ExpertComputerPlayer;
