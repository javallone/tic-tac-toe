import ReactModal from 'react-modal';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

ReactModal.defaultProps.ariaHideApp = false;
Enzyme.configure({ adapter: new Adapter() });
