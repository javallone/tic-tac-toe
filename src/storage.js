export const loadStorage = (name, fallback = null) => {
  if (!Object.keys(localStorage).includes(name)) {
    return fallback;
  }

  try {
    return JSON.parse(localStorage.getItem(name));
  }
  catch {
    return fallback;
  }
};

export const saveStorage = (name, data) => {
  localStorage.setItem(name, JSON.stringify(data));
};

export const removeStorage = (name) => {
  localStorage.removeItem(name);
};
