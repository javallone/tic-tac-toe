import { uniqueNamesGenerator, colors, animals } from 'unique-names-generator';

const nameGenerator = () => uniqueNamesGenerator({
  dictionaries: [colors, animals],
  separator: ' ',
  style: 'capital'
});

export default nameGenerator;
