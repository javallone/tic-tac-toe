import React from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';

import './global.css';

import Header from 'components/Header';
import Game from 'components/Game';

import { StoreProvider } from 'store';

document.body.setAttribute('data-version', process.env.CI_COMMIT_SHORT_SHA);
document.body.setAttribute('data-build-env', process.env.NODE_ENV);
document.body.setAttribute('data-build-time', process.env.BUILD_TIME);

const appContainer = document.getElementById('container');
ReactModal.setAppElement(appContainer);
ReactDOM.render(
  <StoreProvider>
    <Header/>
    <Game/>
  </StoreProvider>,
  appContainer
);

if ('serviceWorker' in navigator && process.env.NODE_ENV === 'production') {
  window.addEventListener('load', async () => {
    try {
      const registration = await navigator.serviceWorker.register('/service-worker.js');
      console.log('SW loaded:', registration);
    }
    catch (error) {
      console.log('SW registration failed:', error);
    }
  });
}
