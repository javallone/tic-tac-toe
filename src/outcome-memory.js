import { PLAYER_TO_BIPOLAR, BIPOLAR_TO_PLAYER } from 'app-constants';
import { loadStorage, saveStorage, removeStorage } from 'storage';

class OutcomeMemory {
  constructor(storageKey) {
    this.storageKey = storageKey;
  }

  loadStorage() {
    this.memory = loadStorage(this.storageKey, {});

    console.log(`Loaded memory: ${Object.keys(this.memory).length} known outcomes`);
  }

  saveStorage() {
    saveStorage(this.storageKey, this.memory);

    console.log(`Saved memory: ${Object.keys(this.memory).length} known outcomes`);
  }

  clearStorage() {
    this.memory = {};
    removeStorage(this.storageKey);

    console.log('Cleared memory');
  }

  learn(state, result) {
    const storedResult = PLAYER_TO_BIPOLAR[result];

    if (!state || this.memory[state.hash] === storedResult) {
      return;
    }

    if (this.memory[state.hash] === undefined) {
      console.log(`Learning ${state.hash} has outcome ${result.toString()}`);
    } else {
      console.log(`Re-learning ${state.hash} has outcome ${result.toString()}, not ${BIPOLAR_TO_PLAYER[this.memory[state.hash]].toString()}`);
    }

    this.memory[state.hash] = storedResult;
    this.saveStorage();
  }

  outcome(state) {
    return state.isComplete ? state.winner : BIPOLAR_TO_PLAYER[this.memory[state.hash]];
  }
}

export default OutcomeMemory;
