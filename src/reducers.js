import {
  SETTINGS_STORE,
  CHANGE_SETTINGS,
  CHANGE_STATE,
  INITIALIZE,
  MEMORY_STORE,
  SET_PAUSED,
  START_GAME,
  PLAYER_X,
  PLAYER_O
} from 'app-constants';
import { PLAYER_CLASS } from 'player';
import GameState from 'game-state';
import OutcomeMemory from 'outcome-memory';
import { saveStorage, loadStorage } from 'storage';
import nameGenerator from 'name-generator';

const reducers = {
  [CHANGE_SETTINGS]: (state, { settings }) => {
    saveStorage(SETTINGS_STORE, settings);
    return {
      ...state,
      settings
    };
  },

  [CHANGE_STATE]: (state, { gameState }) => {
    if (!gameState) {
      return state;
    }

    return {
      ...state,
      gameState
    };
  },

  [INITIALIZE]: (state) => {
    if (state.initialized) {
      return state;
    }

    const memory = new OutcomeMemory(MEMORY_STORE);
    memory.loadStorage();

    return reducers[START_GAME]({
      initialized: true,
      memory,
      paused: false,
      settings: {
        players: [
          { type: 'human', name: nameGenerator() },
          { type: 'learning', name: nameGenerator() }
        ],
        randomPlay: false,
        resetDelay: 2000,
        ...loadStorage(SETTINGS_STORE, {})
      },
    });
  },

  [SET_PAUSED]: (state, { paused }) => {
    return {
      ...state,
      paused
    };
  },

  [START_GAME]: (state) => {
    const { memory, settings } = state;
    const assignmentOrder = (!settings.randomPlay || Math.floor(Math.random() * 2))
      ? [PLAYER_X, PLAYER_O]
      : [PLAYER_O, PLAYER_X];
    const players = assignmentOrder.reduce((result, player, i) => ({
      ...result,
      [player]: new PLAYER_CLASS[settings.players[i].type](player, {
        name: settings.players[i].name,
        memory
      })
    }), {});

    return {
      ...state,
      gameState: new GameState(),
      players
    };
  }
};

export default reducers;
