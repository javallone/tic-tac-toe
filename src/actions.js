import {
  CHANGE_SETTINGS,
  CHANGE_STATE,
  INITIALIZE,
  SET_PAUSED,
  START_GAME
} from 'app-constants';

export const changeSettings = (settings) => ({ type: CHANGE_SETTINGS, settings });
export const changeState = (gameState) => ({ type: CHANGE_STATE, gameState });
export const initialize = () => ({ type: INITIALIZE });
export const setPaused = (paused) => ({ type: SET_PAUSED, paused: !!paused });
export const startGame = () => ({ type: START_GAME });
