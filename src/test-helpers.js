import GameState from 'game-state';
import { PLAYER_X, PLAYER_O, PLAYER_NONE } from 'app-constants';

const STR_TO_PLAYER = {
  '.': PLAYER_NONE,
  'X': PLAYER_X,
  'O': PLAYER_O
};
export const strToBoard = (str) => new GameState([...str]
  .filter((s) => Object.keys(STR_TO_PLAYER).includes(s))
  .map((s) => STR_TO_PLAYER[s]));
