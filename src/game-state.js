import {
  PLAYER_NONE,
  PLAYER_X,
  PLAYER_O,
  PLAYER_TO_BIPOLAR,
  BIPOLAR_TO_PLAYER
} from 'app-constants';

const STATE_CACHE = {};
const MOVES_CACHE = {};
const SCORE_CACHE = {};

const HASH_PATTERNS = Object.freeze([
  Object.freeze([
    0, 1, 2,
    3, 4, 5,
    6, 7, 8
  ]),
  Object.freeze([
    6, 3, 0,
    7, 4, 1,
    8, 5, 2
  ]),
  Object.freeze([
    8, 7, 6,
    5, 4, 3,
    2, 1, 0
  ]),
  Object.freeze([
    2, 5, 8,
    1, 4, 7,
    0, 3, 6
  ]),
  Object.freeze([
    2, 1, 0,
    5, 4, 3,
    8, 7, 6
  ]),
  Object.freeze([
    8, 5, 2,
    7, 4, 1,
    6, 3, 0
  ]),
  Object.freeze([
    6, 7, 8,
    3, 4, 5,
    0, 1, 2
  ]),
  Object.freeze([
    0, 3, 6,
    1, 4, 7,
    2, 5, 8
  ])
]);

const PLAYER_TO_INT = Object.freeze({
  [PLAYER_NONE]: 0,
  [PLAYER_X]: 1,
  [PLAYER_O]: 2
});

const CACHE_MAP = Object.freeze({
  [PLAYER_NONE]: '.',
  [PLAYER_X]: 'X',
  [PLAYER_O]: 'O'
});

const WIN_PATTERNS = Object.freeze({
  row0: Object.freeze([0, 1, 2]),
  row1: Object.freeze([3, 4, 5]),
  row2: Object.freeze([6, 7, 8]),
  col0: Object.freeze([0, 3, 6]),
  col1: Object.freeze([1, 4, 7]),
  col2: Object.freeze([2, 5, 8]),
  diaga: Object.freeze([0, 4, 8]),
  diagb: Object.freeze([6, 4, 2])
});

const EMPTY_BOARD = Object.freeze([...Array(9)].map(() => PLAYER_NONE));

class GameState {
  constructor(board = EMPTY_BOARD) {
    this.cacheKey = board.map((b) => CACHE_MAP[b]).join('');
    if (STATE_CACHE[this.cacheKey]) {
      return STATE_CACHE[this.cacheKey];
    }

    this.board = Object.freeze([...board]);
    this.player = this.board.reduce((sum, x) => sum + PLAYER_TO_BIPOLAR[x], 0) ? PLAYER_O : PLAYER_X;

    const win = Object.entries(WIN_PATTERNS)
      .map(([name, pos]) => [
        name,
        BIPOLAR_TO_PLAYER[Math.trunc(pos.reduce((sum, p) => sum + PLAYER_TO_BIPOLAR[this.playerAt(p)], 0) / 3)]
      ])
      .find((x) => x[1] !== PLAYER_NONE);

    [this.winDirection, this.winner] = win || ['', PLAYER_NONE];
    this.isStalemate = this.winner === PLAYER_NONE && this.board.reduce((sum, x) => sum + Math.abs(PLAYER_TO_BIPOLAR[x]), 0) === 9;
    this.isComplete = this.winner !== PLAYER_NONE || this.isStalemate;

    this.hash = HASH_PATTERNS.map(
      (hash) => hash
        .map((pos) => PLAYER_TO_INT[this.board[pos]])
        .reduce((sum, v, i) => sum + v * Math.pow(3, i), 0)
    ).sort((a, b) => a - b)[0].toString(16).padStart(4, '0');

    STATE_CACHE[this.cacheKey] = Object.freeze(this);
  }

  get moves() {
    if (this.isComplete) {
      return [];
    }

    if (!MOVES_CACHE[this.cacheKey]) {
      MOVES_CACHE[this.cacheKey] = Object.freeze(this.board.map((player, pos) => player !== PLAYER_NONE
        ? null
        : this.moveAt(pos)
      ).filter(Boolean));
    }

    return MOVES_CACHE[this.cacheKey];
  }

  get score() {
    if (SCORE_CACHE[this.hash] === undefined) {
      if (this.isComplete) {
        SCORE_CACHE[this.hash] = PLAYER_TO_BIPOLAR[this.winner];
      } else {
        const idealMove = this.moves.find((move) => move.score === PLAYER_TO_BIPOLAR[this.player]);

        if (idealMove) {
          SCORE_CACHE[this.hash] = idealMove.score;
        } else {
          SCORE_CACHE[this.hash] = this.moves.reduce((sum, move) => sum + move.score, 0) / this.moves.length;
        }
      }
    }

    return SCORE_CACHE[this.hash];
  }

  playerAt(pos) {
    return this.board[pos];
  }

  moveAt(pos) {
    if (this.isComplete) {
      throw new Error('Attempted to move after complete');
    }

    if (this.board[pos] !== PLAYER_NONE) {
      throw new Error('Attempted to move in occupied space');
    }

    const board = [...this.board];
    board[pos] = this.player;
    return new GameState(board);
  }
}

export default GameState;
