import React, { useState, useCallback } from 'react';

import SettingsIcon from 'feather-icons/dist/icons/settings.svg';

import { PLAYER_X, PLAYER_O } from 'app-constants';
import SettingsModal from 'components/SettingsModal';
import { useStore } from 'store';

import style from './style.module.css';

const Header = () => {
  const { gameState, players } = useStore();
  const [settingsOpen, setSettingsOpen] = useState(false);
  const handleOpenSettings = useCallback((event) => {
    event.preventDefault();

    setSettingsOpen(true);
  }, [setSettingsOpen]);
  const handleCloseSettings = useCallback(() => setSettingsOpen(false), [setSettingsOpen]);

  return <>
    <header className={ style.header }>
      <ol>
        { [players[PLAYER_X], players[PLAYER_O]].map((player, i) => (
          <li key={ i } className={ gameState.player === player.player ? style.active : null }>
            <div className={ style.playerName }>{ player.name }</div>
            <div className={ style.playerStatus }>{ [
              player.constructor.description,
              player.status
            ].filter(Boolean).join('; ') }</div>
          </li>
        )) }
      </ol>

      <button className={ style.showSettings } onClick={ handleOpenSettings }>
        <SettingsIcon/>
      </button>
    </header>

    <SettingsModal isOpen={ settingsOpen } onRequestClose={ handleCloseSettings }/>
  </>;
};

export default Header;
