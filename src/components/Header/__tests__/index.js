import React from 'react';
import { shallow } from 'enzyme';

import { PLAYER_X, PLAYER_O } from 'app-constants';
import Header from 'components/Header';
import HumanPlayer from 'player/human-player';
import ExpertComputerPlayer from 'player/expert-computer-player';
import { useStore } from 'store';

jest.mock('store');

describe('<Header>', () => {
  let store;

  beforeEach(() => {
    store = {
      gameState: {},
      players: {
        [PLAYER_X]: new HumanPlayer(PLAYER_X, { name: 'Player 1' }),
        [PLAYER_O]: new ExpertComputerPlayer(PLAYER_O, { name: 'Player 2' })
      }
    };
    useStore.mockImplementation(() => store);
  });

  it('renders with X as active player', () => {
    store.gameState.player = PLAYER_X;
    const wrapper = shallow(
      <Header/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders with O as active player', () => {
    store.gameState.player = PLAYER_O;
    const wrapper = shallow(
      <Header/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders player statuses', () => {
    store.players[PLAYER_X].status = 'Player 1 status';
    store.players[PLAYER_O].status = 'Player 2 status';
    const wrapper = shallow(
      <Header/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('opens the SettingsModal when the settings button is clicked', () => {
    const wrapper = shallow(
      <Header/>
    );

    expect(wrapper).toMatchSnapshot();

    const clickArgs = { preventDefault: jest.fn() };
    wrapper.find('button.showSettings').simulate('click', clickArgs);

    expect(clickArgs.preventDefault).toHaveBeenCalled();
    expect(wrapper).toMatchSnapshot();
  });

  it('closes the SettingsModal when modal requests', () => {
    const wrapper = shallow(
      <Header/>
    );

    wrapper.find('button.showSettings').simulate('click', { preventDefault: jest.fn() });
    expect(wrapper).toMatchSnapshot();
    wrapper.find('SettingsModal').simulate('requestClose');
    expect(wrapper).toMatchSnapshot();
  });
});
