import React from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';

import CloseIcon from 'feather-icons/dist/icons/x-circle.svg';

import style from './style.module.css';

const ANIM_DURATION = 300;

const Modal = ({
  children,
  className,
  onRequestClose,
  overlayClassName,
  ...props
}) => {
  return <ReactModal
    closeTimeoutMS={ ANIM_DURATION }
    className={ [style.modal, className].filter(Boolean).join(' ') }
    onRequestClose={ onRequestClose }
    overlayClassName={ [style.overlay, overlayClassName].filter(Boolean).join(' ') }
    style={{ content: { animationDuration: ANIM_DURATION + 'ms' } }}
    { ...props }>

    { children }

    <button type="button" className={ style.closeBtn } onClick={ onRequestClose }>
      <CloseIcon/>
    </button>
  </ReactModal>;
};

Modal.propTypes = {
  className: PropTypes.string,
  onRequestClose: PropTypes.func,
  overlayClassName: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default Modal;
