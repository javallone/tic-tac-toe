import React from 'react';
import { shallow } from 'enzyme';

import Modal from 'components/Modal';

describe('<Modal>', () => {
  it('renders', () => {
    const wrapper = shallow(
      <Modal isOpen={ true }>
        <p>Content</p>
      </Modal>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('requests the modal be closed when the close button is clicked', () => {
    const onRequestClose = jest.fn();
    const wrapper = shallow(
      <Modal isOpen={ true } onRequestClose={ onRequestClose }>
        <p>Content</p>
      </Modal>
    );

    wrapper.find('.closeBtn').simulate('click');
    expect(onRequestClose).toHaveBeenCalled();
  });
});
