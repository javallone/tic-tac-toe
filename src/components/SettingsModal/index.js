import React, { useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { changeSettings, startGame, setPaused, } from 'actions';
import { useDispatch, useStore } from 'store';

import Modal from 'components/Modal';
import StatusModal from 'components/StatusModal';
import PlayerRow from './player-row';

import style from './style.module.css';

const SettingsModal = ({ onRequestClose, ...props }) => {
  const player1El = useRef(null);
  const player2El = useRef(null);
  const randomPlayEl = useRef(null);
  const resetDelayEl = useRef(null);
  const resetEl = useRef(null);

  const { settings: storeSettings, memory } = useStore();
  const dispatch = useDispatch();

  const [settings, setSettings] = useState(storeSettings);
  const [reset, setReset] = useState(true);
  const [memoryClearedOpen, setMemoryClearedOpen] = useState(false);

  const getCurrentSettings = useCallback(() => {
    return {
      players: [
        player1El.current.value,
        player2El.current.value
      ],
      randomPlay: randomPlayEl.current.checked,
      resetDelay: Number(resetDelayEl.current.value) * 1000
    };
  }, [player1El, player2El, randomPlayEl, resetDelayEl]);
  const handleChange = useCallback(() => {
    setSettings(getCurrentSettings());
  }, [setSettings, getCurrentSettings]);
  const handleResetChange = useCallback(() => {
    setReset(resetEl.current.checked);
  }, [setReset, resetEl]);

  const handleSaveSettings = useCallback((event) => {
    event.preventDefault();

    dispatch(changeSettings(settings));

    if (reset) {
      dispatch(startGame());
    }

    if (onRequestClose) {
      onRequestClose();
    }
  }, [onRequestClose, reset, settings, dispatch]);

  const handleAfterOpen = useCallback(() => {
    dispatch(setPaused(true));
  }, [dispatch]);
  const handleAfterClose = useCallback(() => {
    setReset(true);
    setSettings(storeSettings);
    dispatch(setPaused(false));
  }, [dispatch, setReset, setSettings, storeSettings]);

  const handleClearMemory = useCallback(() => {
    memory.clearStorage();
    setMemoryClearedOpen(true);
  }, [memory, setMemoryClearedOpen]);

  const handleMemoryClearedClose = useCallback(() => {
    setMemoryClearedOpen(false);
  }, [setMemoryClearedOpen]);

  return <Modal
    overlayClassName={ style.overlay }
    onAfterOpen={ handleAfterOpen }
    onAfterClose={ handleAfterClose }
    onRequestClose={ onRequestClose }
    { ...props }>
    <form onSubmit={ handleSaveSettings }>
      <div className={ style.content }>
        <table>
          <tbody>
            <tr>
              <th>#</th>
              <th>Type</th>
              <th colSpan="2">Name</th>
            </tr>
            <PlayerRow ref={ player1El } number="1" player={ settings.players[0] } onChange={ handleChange }/>
            <PlayerRow ref={ player2El } number="2" player={ settings.players[1] } onChange={ handleChange }/>
          </tbody>
        </table>
        <hr/>
        <label>
          <input type="checkbox" ref={ randomPlayEl } checked={ settings.randomPlay } onChange={ handleChange }/>
          &nbsp;Random play order?
        </label>
        <label className={ style.resetDelay }>
          Reset delay&nbsp;
          <input type="number" min="0" ref={ resetDelayEl } value={ settings.resetDelay / 1000 } onChange={ handleChange }/> seconds
        </label>
      </div>

      <div className={ style.buttonRow }>
        <div>
          <button type="submit">Save</button>
          <label>
            <input type="checkbox" ref={ resetEl } checked={ reset } onChange={ handleResetChange }/>
            &nbsp;Reset?
          </label>
        </div>
        <button type="button" onClick={ handleClearMemory }>Clear Memory</button>
      </div>
    </form>

    <StatusModal
      overlayClassName={ style.statusOverlay }
      message="Memory Cleared"
      autoClose={ 2000 }
      isOpen={ memoryClearedOpen }
      onRequestClose={ handleMemoryClearedClose }/>
  </Modal>;
};

SettingsModal.propTypes = {
  onRequestClose: PropTypes.func
};

export default SettingsModal;
