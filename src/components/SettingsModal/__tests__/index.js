import React from 'react';
import { shallow, mount } from 'enzyme';

import {
  CHANGE_SETTINGS,
  SET_PAUSED,
  START_GAME
} from 'app-constants';
import SettingsModal from 'components/SettingsModal';
import { useStore, useDispatch } from 'store';

jest.mock('store');

describe('<SettingsModal>', () => {
  let dispatch;
  let storeData;

  beforeEach(() => {
    storeData = {
      settings: {
        players: [
          { type: 'human', name: 'Player 1' },
          { type: 'human', name: 'Player 2' }
        ],
        randomPlay: true,
        resetDelay: 2000
      },
      memory: {
        clearStorage: jest.fn()
      }
    };
    useStore.mockImplementation(() => storeData);

    dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);
  });

  it('renders', () => {
    const wrapper = shallow(
      <SettingsModal isOpen={ true }/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('dispatches a pause action after opening', () => {
    const wrapper = shallow(
      <SettingsModal isOpen={ true }/>
    );

    wrapper.find('Modal').simulate('afterOpen');
    expect(dispatch).toHaveBeenCalledWith({ type: SET_PAUSED, paused: true });
  });

  it('dispatches a pause action after closing', () => {
    const wrapper = shallow(
      <SettingsModal isOpen={ true }/>
    );

    wrapper.find('Modal').simulate('afterClose');
    expect(dispatch).toHaveBeenCalledWith({ type: SET_PAUSED, paused: false });
  });

  it('clears the memory storage and opens the "Memory Cleared" message when "Clear Memory" is clicked', () => {
    const wrapper = shallow(
      <SettingsModal isOpen={ true }/>
    );

    wrapper.findWhere((n) => n.text() === 'Clear Memory').find('button').simulate('click');
    expect(storeData.memory.clearStorage).toHaveBeenCalled();
    expect(wrapper.find('StatusModal')).toMatchSnapshot();
  });

  it('closes the "Memory Cleared" message when the moal requests it', () => {
    const wrapper = shallow(
      <SettingsModal isOpen={ true }/>
    );

    wrapper.findWhere((n) => n.text() === 'Clear Memory').find('button').simulate('click');
    expect(wrapper.find('StatusModal')).toMatchSnapshot();
    wrapper.find('StatusModal').simulate('requestClose');
    expect(wrapper.find('StatusModal')).toMatchSnapshot();
  });

  describe('submitting the settings form', () => {
    it('prevents the default form submit', () => {
      const wrapper = shallow(
        <SettingsModal isOpen={ true }/>
      );

      const preventDefault = jest.fn();
      wrapper.find('form').simulate('submit', { preventDefault });
      expect(preventDefault).toHaveBeenCalled();
    });

    it('dispatches a changeSettings action', () => {
      const wrapper = mount(
        <SettingsModal isOpen={ true }/>
      );

      const players = wrapper.find('table select');
      players.at(0).instance().value = 'expert';
      players.at(0).simulate('change');
      players.at(1).instance().value = 'simple';
      players.at(1).simulate('change');
      const randomPlay = wrapper.find('input[type="checkbox"]').first();
      randomPlay.instance().checked = false;
      randomPlay.simulate('change');
      const resetDelay = wrapper.find('input[type="number"]');
      resetDelay.instance().value = 3;
      resetDelay.simulate('change');

      wrapper.find('form').simulate('submit', { preventDefault: jest.fn() });
      expect(dispatch).toHaveBeenCalledWith({
        type: CHANGE_SETTINGS,
        settings: {
          players: [
            { type: 'expert', name: 'Player 1' },
            { type: 'simple', name: 'Player 2' }
          ],
          randomPlay: false,
          resetDelay: 3000
        }
      });
    });

    it('dispatches a startGame action when "Reset?" checkbox is checked', () => {
      const wrapper = mount(
        <SettingsModal isOpen={ true }/>
      );

      const resetCheckbox = wrapper.find('.buttonRow input[type="checkbox"]');
      resetCheckbox.instance().checked = true;
      resetCheckbox.simulate('change');
      wrapper.find('form').simulate('submit', { preventDefault: jest.fn() });
      expect(dispatch).toHaveBeenCalledWith({ type: START_GAME });
    });

    it('does not dispatch a startGame action when "Reset?" checkbox is unchecked', () => {
      const wrapper = mount(
        <SettingsModal isOpen={ true }/>
      );

      const resetCheckbox = wrapper.find('.buttonRow input[type="checkbox"]');
      resetCheckbox.instance().checked = false;
      resetCheckbox.simulate('change');
      wrapper.find('form').simulate('submit', { preventDefault: jest.fn() });
      expect(dispatch).not.toHaveBeenCalledWith({ type: START_GAME });
    });

    it('request the modal be closed', () => {
      const onRequestClose = jest.fn();
      const wrapper = shallow(
        <SettingsModal isOpen={ true } onRequestClose={ onRequestClose }/>
      );

      wrapper.find('form').simulate('submit', { preventDefault: jest.fn() });
      expect(onRequestClose).toHaveBeenCalled();
    });
  });
});
