import React from 'react';
import { shallow, mount } from 'enzyme';

import PlayerRow from 'components/SettingsModal/player-row';

describe('<PlayerRow>', () => {
  it('renders', () => {
    const wrapper = shallow(
      <PlayerRow number="1" player={{ type: 'human', name: 'Example player' }}/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('calls the onChange callback when the player name is changed', () => {
    const onChange = jest.fn();
    const wrapper = mount(
      <table>
        <tbody>
          <PlayerRow
            number="1"
            player={{ type: 'human', name: 'Example player' }}
            onChange={ onChange }
          />
        </tbody>
      </table>
    );

    wrapper.find('input[type="text"]').simulate('change');
    jest.runAllTimers();
    expect(onChange).toHaveBeenCalled();
  });

  it('calls the onChange callback when the player type is changed', () => {
    const onChange = jest.fn();
    const wrapper = mount(
      <table>
        <tbody>
          <PlayerRow
            number="1"
            player={{ type: 'human', name: 'Example player' }}
            onChange={ onChange }
          />
        </tbody>
      </table>
    );

    wrapper.find('select').simulate('change');
    jest.runAllTimers();
    expect(onChange).toHaveBeenCalled();
  });

  it('calls the onChange callback when the refresh button is clicked', () => {
    const onChange = jest.fn();
    const wrapper = mount(
      <table>
        <tbody>
          <PlayerRow
            number="1"
            player={{ type: 'human', name: 'Example player' }}
            onChange={ onChange }
          />
        </tbody>
      </table>
    );

    wrapper.find('button').simulate('click');
    jest.runAllTimers();
    expect(onChange).toHaveBeenCalled();
  });

  it('updates the current value when the player name is changed', () => {
    const ref = React.createRef();
    const wrapper = mount(
      <table>
        <tbody>
          <PlayerRow
            ref={ ref }
            number="1"
            player={{ type: 'human', name: 'Example player' }}
          />
        </tbody>
      </table>
    );

    const input = wrapper.find('input[type="text"]');
    input.instance().value = 'New player name';
    input.simulate('change');
    jest.runAllTimers();
    expect(ref.current.value).toEqual({ type: 'human', name: 'New player name' });
  });

  it('updates the current value when the player type is changed', () => {
    const ref = React.createRef();
    const wrapper = mount(
      <table>
        <tbody>
          <PlayerRow
            ref={ ref }
            number="1"
            player={{ type: 'human', name: 'Example player' }}
          />
        </tbody>
      </table>
    );

    const select = wrapper.find('select');
    select.instance().value = 'simple';
    select.simulate('change');
    jest.runAllTimers();
    expect(ref.current.value).toEqual({ type: 'simple', name: 'Example player' });
    select.instance().value = 'learning';
    select.simulate('change');
    jest.runAllTimers();
    expect(ref.current.value).toEqual({ type: 'learning', name: 'Example player' });
    select.instance().value = 'expert';
    select.simulate('change');
    jest.runAllTimers();
    expect(ref.current.value).toEqual({ type: 'expert', name: 'Example player' });
    select.instance().value = 'human';
    select.simulate('change');
    jest.runAllTimers();
    expect(ref.current.value).toEqual({ type: 'human', name: 'Example player' });
  });

  it('generates a new player name when the refresh button is clicked', () => {
    const ref = React.createRef();
    const wrapper = mount(
      <table>
        <tbody>
          <PlayerRow
            ref={ ref }
            number="1"
            player={{ type: 'human', name: 'Example player' }}
          />
        </tbody>
      </table>
    );

    wrapper.find('button').simulate('click');
    jest.runAllTimers();
    expect(ref.current.value).toEqual({ type: 'human', name: expect.any(String) });
  });
});
