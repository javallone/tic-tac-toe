import React, { useCallback, useImperativeHandle, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import RefreshIcon from 'feather-icons/dist/icons/refresh-cw.svg';

import { PLAYER_CLASS } from 'player';
import nameGenerator from 'name-generator';

const PlayerRow = React.forwardRef(({ number, player, onChange }, ref) => {
  const typeEl = useRef(null);
  const nameEl = useRef(null);

  const [type, setType] = useState(player.type);
  const [name, setName] = useState(player.name);

  const handleTypeChange = useCallback(() => {
    setType(typeEl.current.value);
    onChange && setTimeout(() => onChange());
  }, [setType, typeEl, onChange]);

  const handleNameChange = useCallback(() => {
    setName(nameEl.current.value);
    onChange && setTimeout(() => onChange());
  }, [setName, nameEl, onChange]);

  const handleRefreshClick = useCallback((event) => {
    event.preventDefault();
    setName(nameGenerator());
    onChange && setTimeout(() => onChange());
  }, [setName, onChange]);

  useImperativeHandle(ref, () => ({
    get value() {
      return { type, name };
    }
  }), [type, name]);

  return <tr>
    <td>{ number }</td>
    <td>
      <select ref={ typeEl } value={ type } onChange={ handleTypeChange }>
        { Object.entries(PLAYER_CLASS).map(([value, type], i) => (
          <option key={ i } value={ value }>{ type.description }</option>
        )) }
      </select>
    </td>
    <td>
      <input type="text" required ref={ nameEl } value={ name } onChange={ handleNameChange }/>
    </td>
    <td>
      <button type="button" onClick={ handleRefreshClick }><RefreshIcon/></button>
    </td>
  </tr>;
});

PlayerRow.displayName = 'PlayerRow';

PlayerRow.propTypes = {
  number: PropTypes.string.isRequired,
  player: PropTypes.shape({
    type: PropTypes.string,
    name: PropTypes.string
  }).isRequired,
  onChange: PropTypes.func
};

export default PlayerRow;
