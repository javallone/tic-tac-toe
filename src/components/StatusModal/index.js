import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import Modal from 'components/Modal';

import style from './style.module.css';

const StatusModal = ({ message, autoClose, paused, onRequestClose, ...props }) => {
  const onAnimationEnd = useCallback(() => {
    onRequestClose && onRequestClose();
  }, [onRequestClose]);

  return <Modal
    onRequestClose={ onRequestClose }
    { ...props }
  >
    <p className={ style.message}>{ message }</p>

    { (!!autoClose) && <div className={ style.countdown }>
      <div
        onAnimationEnd={ onAnimationEnd }
        style={{ animation: !paused && `${autoClose}ms linear ${style.statusCountdown}` }}
      ></div>
    </div> }
  </Modal>;
};

StatusModal.propTypes = {
  message: PropTypes.string,
  autoClose: PropTypes.number,
  paused: PropTypes.bool,
  onRequestClose: PropTypes.func
};

export default StatusModal;
