import React from 'react';
import { shallow } from 'enzyme';

import StatusModal from 'components/StatusModal';

describe('<StatusModal>', () => {
  it('renders with the animation running when not paused', () => {
    const wrapper = shallow(
      <StatusModal isOpen={ true } message="Example message" autoClose={ 2000 }/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders without the animation running when paused', () => {
    const wrapper = shallow(
      <StatusModal isOpen={ true } message="Example message" autoClose={ 2000 } paused/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders without a countdown bar when autoClose is falsy', () => {
    const wrapper = shallow(
      <StatusModal isOpen={ true } message="Example message" autoClose={ 0 }/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('requests the modal be closed when the animation is complete', () => {
    const onRequestClose = jest.fn();
    const wrapper = shallow(
      <StatusModal isOpen={ true } message="Example message" autoClose={ 2000 } onRequestClose={ onRequestClose }/>
    );

    wrapper.find('.countdown > div').simulate('animationEnd');
    expect(onRequestClose).toHaveBeenCalled();
  });

  it('protects against on onRequestClose callback being defined', () => {
    const wrapper = shallow(
      <StatusModal isOpen={ true } message="Example message" autoClose={ 2000 }/>
    );

    expect(() => wrapper.find('.countdown > div').simulate('animationEnd')).not.toThrow();
  });
});
