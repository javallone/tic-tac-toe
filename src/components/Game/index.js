import React, { useCallback, useEffect } from 'react';

import StatusModal from 'components/StatusModal';

import {
  PLAYER_NONE,
  PLAYER_X,
  PLAYER_O
} from 'app-constants';

import { changeState, startGame } from 'actions';
import { useDispatch, useStore } from 'store';

import style from './style.module.css';

const SPACE_CENTERS = [
  { transform: 'translate(50, 50)' },
  { transform: 'translate(150, 50)' },
  { transform: 'translate(250, 50)' },
  { transform: 'translate(50, 150)' },
  { transform: 'translate(150, 150)' },
  { transform: 'translate(250, 150)' },
  { transform: 'translate(50, 250)' },
  { transform: 'translate(150, 250)' },
  { transform: 'translate(250, 250)' }
];

const GAME_AREAS = [
  { x:   0, y:   0, width: 100, height: 100 },
  { x: 100, y:   0, width: 100, height: 100 },
  { x: 200, y:   0, width: 100, height: 100 },
  { x:   0, y: 100, width: 100, height: 100 },
  { x: 100, y: 100, width: 100, height: 100 },
  { x: 200, y: 100, width: 100, height: 100 },
  { x:   0, y: 200, width: 100, height: 100 },
  { x: 100, y: 200, width: 100, height: 100 },
  { x: 200, y: 200, width: 100, height: 100 }
];

const GAME_MARKERS = {
  [PLAYER_NONE]: () => {},
  [PLAYER_X]: (props) => <path d="M -30,-30 l 60,60 m 0,-60 l -60,60" { ...props }/>, // eslint-disable-line react/display-name
  [PLAYER_O]: (props) => <circle cx="0" cy="0" r="30" { ...props }/> // eslint-disable-line react/display-name
};

const Game = () => {
  const dispatch = useDispatch();
  const { gameState, players, paused, settings: { resetDelay } } = useStore();

  const currentPlayer = players[gameState.player];
  const statusMessage = gameState.winner === PLAYER_NONE
    ? 'Stalemate'
    : `${players[gameState.winner].name} wins`;

  const clickHandler = useCallback((event) => {
    const pos = Number(event.target.getAttribute('data-pos'));
    dispatch(changeState(currentPlayer.clickHandler(pos, gameState)));
  }, [dispatch, gameState, currentPlayer]);

  const handleStatusClose = useCallback(() => {
    [PLAYER_X, PLAYER_O].forEach((p) => players[p].complete(gameState));
    dispatch(startGame());
  }, [dispatch, players, gameState]);

  useEffect(() => {
    if (gameState.isComplete || paused) {
      return;
    }

    const timer = setTimeout(() => {
      dispatch(changeState(currentPlayer.move(gameState)));
    }, 100);

    return () => clearTimeout(timer);
  }, [dispatch, gameState, currentPlayer, paused]);

  return <>
    <svg className={ style.game } viewBox="-10 -10 320 320" xmlns="http://www.w3.org/2000/svg">
      <line x1="100" y1="0" x2="100" y2="300"/>
      <line x1="200" y1="0" x2="200" y2="300"/>
      <line x1="0" y1="100" x2="300" y2="100"/>
      <line x1="0" y1="200" x2="300" y2="200"/>

      { SPACE_CENTERS.map((props, i) => GAME_MARKERS[gameState.playerAt(i)]({ key: i, ...props })) }

      { (gameState.isComplete && !gameState.isStalemate) &&
        <line
          className={ [style.winHighlight, style[gameState.winDirection]].join(' ') }
          x1="20"
          y1="150"
          x2="280"
          y2="150"
        />
      }

      { GAME_AREAS.map((props, i) => (gameState.playerAt(i) === PLAYER_NONE && <rect key={ i } data-pos={ i } className={ style.gameArea } onClick={ clickHandler } { ...props }/>)) }
    </svg>

    <StatusModal
      message={ statusMessage }
      autoClose={ resetDelay }
      paused={ paused }
      isOpen={ gameState.isComplete }
      onRequestClose={ handleStatusClose }/>
  </>;
};

export default Game;
