import React from 'react';
import { shallow, mount } from 'enzyme';

import {
  CHANGE_STATE,
  PLAYER_X,
  PLAYER_O,
  START_GAME
} from 'app-constants';
import Game from 'components/Game';
import Player from 'player/base';
import { useStore, useDispatch } from 'store';
import { strToBoard } from 'test-helpers';

jest.mock('store');

describe('<Game>', () => {
  let dispatch;
  let storeData;

  beforeEach(() => {
    storeData = {
      gameState: strToBoard('... ... ...'),
      players: {
        [PLAYER_X]: new Player(PLAYER_X, { name: 'Player X' }),
        [PLAYER_O]: new Player(PLAYER_O, { name: 'Player O' })
      },
      paused: false,
      settings: {
        resetDelay: 2000
      }
    };
    useStore.mockImplementation(() => storeData);

    dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);
  });

  it('renders', () => {
    const wrapper = shallow(
      <Game/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders a stalemate', () => {
    storeData.gameState = strToBoard('XOX XOO OXX ');
    const wrapper = shallow(
      <Game/>
    );
    expect(wrapper).toMatchSnapshot();
  });

  [
    ['XXX ... ...', 'X on row 1'],
    ['... XXX ...', 'X on row 2'],
    ['... ... XXX', 'X on row 3'],
    ['X.. X.. X..', 'X on row 1'],
    ['.X. .X. .X.', 'X on row 2'],
    ['..X ..X ..X', 'X on row 3'],
    ['X.. .X. ..X', 'X on left-right diagonal'],
    ['..X .X. X..', 'X on right-left diagonal'],
    ['OOO ... ...', 'O on row 1'],
    ['... OOO ...', 'O on row 2'],
    ['... ... OOO', 'O on row 3'],
    ['O.. O.. O..', 'O on row 1'],
    ['.O. .O. .O.', 'O on row 2'],
    ['..O ..O ..O', 'O on row 3'],
    ['O.. .O. ..O', 'O on left-right diagonal'],
    ['..O .O. O..', 'O on right-left diagonal']
  ].forEach(([str, winDirection]) => {
    it(`renders a highlight for ${winDirection}`, () => {
      storeData.gameState = strToBoard(str);
      const wrapper = shallow(
        <Game/>
      );
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('dispatches a changeState action when a game area is clicked', () => {
    jest.spyOn(storeData.players[PLAYER_X], 'clickHandler').mockReturnValue('Player X click result');
    jest.spyOn(storeData.players[PLAYER_O], 'clickHandler').mockReturnValue('Player O click result');

    const wrapper = mount(
      <Game/>
    );

    const gameArea = wrapper.find('.gameArea').at(5);
    gameArea.simulate('click', { target: gameArea.instance() });
    expect(storeData.players[PLAYER_X].clickHandler).toHaveBeenCalledWith(5, storeData.gameState);
    expect(storeData.players[PLAYER_O].clickHandler).not.toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith({ type: CHANGE_STATE, gameState: 'Player X click result' });
  });

  it('calls the complete handler for players and dispatches a startGame action when the status modal closes', () => {
    jest.spyOn(storeData.players[PLAYER_X], 'complete');
    jest.spyOn(storeData.players[PLAYER_O], 'complete');

    storeData.gameState = strToBoard('XXX ... ...');

    const wrapper = mount(
      <Game/>
    );

    wrapper.find('.closeBtn').simulate('click');
    expect(storeData.players[PLAYER_X].complete).toHaveBeenCalledWith(storeData.gameState);
    expect(storeData.players[PLAYER_O].complete).toHaveBeenCalledWith(storeData.gameState);
    expect(dispatch).toHaveBeenCalledWith({ type: START_GAME });
  });

  it('dispatches a changeState action when the current player has a move handler', () => {
    jest.spyOn(storeData.players[PLAYER_X], 'move').mockReturnValue('Player X move result');
    jest.spyOn(storeData.players[PLAYER_O], 'move').mockReturnValue('Player O move result');

    const wrapper = mount(
      <Game/>
    );

    jest.runAllTimers();

    expect(storeData.players[PLAYER_X].move).toHaveBeenCalledWith(storeData.gameState);
    expect(storeData.players[PLAYER_O].move).not.toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith({ type: CHANGE_STATE, gameState: 'Player X move result' });

    wrapper.unmount();
  });

  it('does not call player move handlers when game is complete', () => {
    jest.spyOn(storeData.players[PLAYER_X], 'move').mockReturnValue('Player X move result');
    jest.spyOn(storeData.players[PLAYER_O], 'move').mockReturnValue('Player O move result');

    storeData.gameState = strToBoard('XXX ... ...');

    const wrapper = mount(
      <Game/>
    );

    jest.runAllTimers();

    expect(storeData.players[PLAYER_X].move).not.toHaveBeenCalled();
    expect(storeData.players[PLAYER_O].move).not.toHaveBeenCalled();
    expect(dispatch).not.toHaveBeenCalled();

    wrapper.unmount();
  });

  it('does not call player move handlers when paused', () => {
    jest.spyOn(storeData.players[PLAYER_X], 'move').mockReturnValue('Player X move result');
    jest.spyOn(storeData.players[PLAYER_O], 'move').mockReturnValue('Player O move result');

    storeData.paused = true;

    const wrapper = mount(
      <Game/>
    );

    jest.runAllTimers();

    expect(storeData.players[PLAYER_X].move).not.toHaveBeenCalled();
    expect(storeData.players[PLAYER_O].move).not.toHaveBeenCalled();
    expect(dispatch).not.toHaveBeenCalled();

    wrapper.unmount();
  });
});
