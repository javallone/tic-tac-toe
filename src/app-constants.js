// localStorage keys
export const SETTINGS_STORE = 'ttt-settings';
export const MEMORY_STORE = 'ttt-memory';

// Reducer actions
export const CHANGE_SETTINGS = Symbol('CHANGE_SETTINGS');
export const CHANGE_STATE = Symbol('CHANGE_STATE');
export const INITIALIZE = Symbol('INITIALIZE');
export const SET_PAUSED = Symbol('SET_PAUSED');
export const START_GAME = Symbol('START_GAME');

// Game state
export const PLAYER_NONE = Symbol('PLAYER_NONE');
export const PLAYER_X = Symbol('PLAYER_X');
export const PLAYER_O = Symbol('PLAYER_O');
export const PLAYER_TO_BIPOLAR = {
  [PLAYER_NONE]: 0,
  [PLAYER_X]: 1,
  [PLAYER_O]: -1
};
export const BIPOLAR_TO_PLAYER = {
  [0]: PLAYER_NONE,
  [1]: PLAYER_X,
  [-1]: PLAYER_O
};
